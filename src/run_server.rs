use actix_http::Error;
use actix_web::HttpRequest;
use crate::build_db;
use actix_http::{Response};
use actix_web::middleware::Logger;
use actix::{Actor, StreamHandler};
use actix_web::{get, post, web, App, HttpServer, http};
use actix_web_actors::ws;
use actix_cors::Cors;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};

lazy_static! {
    static ref INDEX_SCHEMA: (tantivy::Index, tantivy::schema::Schema) =
        build_db::load_db("db_stuff").expect("db build error");
    static ref INDEX: tantivy::Index = INDEX_SCHEMA.0.clone();
    static ref SCHEMA: tantivy::schema::Schema = INDEX_SCHEMA.1.clone();
}

#[derive(Serialize, Deserialize, Debug)]
struct DieseasesQuery {
    fields: Vec<String>,
    query: String,
    limit: Option<usize>,
    #[serde(alias="useAnd")]
    use_and: Option<bool>,
}
#[derive(Serialize, Deserialize, Debug)]
struct DieseasesQueryResponse {
    query: String,
}

#[post("/diseases")]
async fn diseases(body: web::Json<DieseasesQuery>) -> Response {
    let searcher = build_db::get_searcher(&INDEX).expect("Searcher");
    let mut fields = vec![];
    for field in body.fields.iter() {
        fields.push(SCHEMA.get_field(field).unwrap());
    }
    let results = build_db::search(searcher, &INDEX, &SCHEMA, fields, &body.query, body.limit.unwrap_or(10), body.use_and.unwrap_or(false));
    Response::Ok().json(results.unwrap())
}



struct DiseasesWs;

impl Actor for DiseasesWs {
    type Context = ws::WebsocketContext<Self>;
}

/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for DiseasesWs {
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        match msg {
            Ok(ws::Message::Ping(msg)) => ctx.pong(&msg),
            Ok(ws::Message::Text(text)) => {
                let body: DieseasesQuery = match serde_json::from_str(&text) {
                    Ok(q) => q,
                    Err(_) => return
                };
                let searcher = build_db::get_searcher(&INDEX).expect("Searcher");
                let mut fields = vec![];
                for field in body.fields.iter() {
                    fields.push(SCHEMA.get_field(field).unwrap());
                }
                let results = build_db::search(searcher, &INDEX, &SCHEMA, fields, &body.query, body.limit.unwrap_or(10), body.use_and.unwrap_or(false)).unwrap();
                ctx.text(serde_json::to_string(&results).unwrap());
            },
            _ => (),
        }
    }
}

#[get("/ws-diseases")]
async fn diseases_ws_handler(req: HttpRequest, stream: web::Payload) -> Result<Response, Error> {
    let resp = ws::start(DiseasesWs {}, &req, stream);
    resp
}

pub async fn run_server(host_port_str: &str) -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    HttpServer::new(|| {
        App::new()
            .service(diseases)
            .service(diseases_ws_handler)
            .wrap(Logger::default())
            .wrap(Logger::new("%a %{User-Agent}i"))
            .wrap(
                Cors::new() // <- Construct CORS middleware builder
                //   .allowed_origin("https://www.rust-lang.org/")
                  .allowed_methods(vec!["GET", "POST"])
                  .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
                  .allowed_header(http::header::CONTENT_TYPE)
                  .max_age(3600)
                  .finish())
    })
    .bind(host_port_str)?
    .run()
    .await
}
