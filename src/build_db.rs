use tantivy::collector::TopDocs;
use tantivy::query::QueryParser;
use tantivy::schema::*;
use tantivy::Index;
use tantivy::ReloadPolicy;
use indicatif::{ProgressBar, ProgressStyle};

use std::collections::HashMap;

use crate::read_outputs::Disease;

pub fn get_searcher(
    index: &tantivy::Index,
) -> tantivy::Result<tantivy::LeasedItem<tantivy::Searcher>> {
    let reader = index
        .reader_builder()
        .reload_policy(ReloadPolicy::OnCommit)
        .try_into()?;
    let searcher = reader.searcher();
    Ok(searcher)
}

pub fn search(
    searcher: tantivy::LeasedItem<tantivy::Searcher>,
    index: &tantivy::Index,
    schema: &tantivy::schema::Schema,
    schema_fields: Vec<tantivy::schema::Field>,
    query_string: &str,
    limit: usize,
    use_and: bool,
) -> tantivy::Result<HashMap<String, f32>> {
    let mut query_parser = QueryParser::for_index(&index, schema_fields);
    if use_and {
        &query_parser.set_conjunction_by_default();
    }
    let query = query_parser.parse_query(query_string)?;
    let top_docs = searcher.search(&query, &TopDocs::with_limit(limit))?;
    let mut results = HashMap::new();
    for (score, doc_address) in top_docs {
        let retrieved_doc = searcher.doc(doc_address)?;
        results.insert(schema.to_json(&retrieved_doc), score);
    }

    Ok(results)
}

pub fn load_db(db_path: &str) -> tantivy::Result<(tantivy::Index, tantivy::schema::Schema)> {
    let index = Index::open_in_dir(db_path)?;
    let schema = index.schema();
    Ok((index, schema))
}

pub fn create_db(
    db_path: &str,
    diseases: &[Disease],
) -> tantivy::Result<(tantivy::Index, tantivy::schema::Schema)> {


    let _ = std::fs::remove_dir_all(db_path);
    std::fs::create_dir_all(db_path)?;


    let mut schema_builder = Schema::builder();
    schema_builder.add_text_field("name", TEXT | STORED);
    schema_builder.add_text_field("about", TEXT | STORED);
    schema_builder.add_text_field("symptoms", TEXT | STORED);
    schema_builder.add_text_field("people_at_risk", TEXT | STORED);
    let schema = schema_builder.build();
    let index = Index::create_in_dir(db_path, schema.clone())?;
    let mut index_writer = index.writer(50_000_000)?;
    let name = schema.get_field("name").unwrap();
    let about = schema.get_field("about").unwrap();
    let symptoms = schema.get_field("symptoms").unwrap();
    let people_at_risk = schema.get_field("people_at_risk").unwrap();

    let pb = ProgressBar::new(diseases.len() as u64);
    pb.set_style(ProgressStyle::default_bar()
        .template("{spinner:.green} [{percent}%] [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} {msg} ({eta_precise})")
        .progress_chars("#>-"));

    pb.enable_steady_tick(100);

    for (i, disease) in diseases.iter().enumerate() {
        let mut disease_doc = Document::default();
        disease_doc.add_text(name, &disease.name);
        let abouts = &disease.standard_sections.about;
        if let Some(abouts) = abouts {
            for about_content in abouts.values() {
                disease_doc.add_text(about, about_content);
            }
        }
        let symptoms_list = &disease.standard_sections.symptoms;
        if let Some(symptoms_list) = symptoms_list {
            for content in symptoms_list.values() {
                disease_doc.add_text(symptoms, content);
            }
        }
        let people_at_risk_list = &disease.standard_sections.people_at_risk;
        if let Some(people_at_risk_list) = people_at_risk_list {
            for content in people_at_risk_list.values() {
                disease_doc.add_text(people_at_risk, content);
            }
        }
        index_writer.add_document(disease_doc);
        let op_stamp = index_writer.commit()?;
        // println!("{}/{} op: {}", i+1, diseases.len(), op_stamp);
        pb.set_position((i+1) as u64);
        pb.set_message(&format!("op: {}", op_stamp));
    }

    pb.finish();
    Ok((index, schema))
}
